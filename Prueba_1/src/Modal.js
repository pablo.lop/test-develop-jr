import React, { Component }from "react";
import { ReactDOM } from "react";
import { Modal, Button, ModalHeader, ModalBody, ModalFooter } from "react-bootstrap";

 class Ventana extends React.Component {

    constructor(){
        super()
        this.state ={
            showModal: false
        }
        
    }

    handleModal(){
        this.setSate({showModal: !this.state.showModal})
    }
    render (){
        return(
            <div>
                <Button onClick={() => this.handleModal()}>Abrir modal</Button>

                <Modal show = {this.state.showModal} onHide={() => this.handleModal()}>
                    <Modal.Header closeButton>
                        Modal Header
                    </Modal.Header>
                    <Modal.Body>
                        Body del modal
                    </Modal.Body>
                    <Modal.Footer>
                        <Button>Aceptar</Button>
                        <Button>Cancelar</Button>
                    </Modal.Footer>

                </Modal>
            </div>
            

    )}

}
export default Ventana;