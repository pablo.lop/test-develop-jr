    import React, {useState, useEffect} from 'react';
    import { Button, Modal, Table, Card, Form, Container, ul } from 'react-bootstrap'
    import './App.css'
    import axios from 'axios'
        function Main() {
            const [estado, setEstado] = useState('')

    const handleChange = event => {
        setEstado({ name: event.target.value });
    }

    const sendForm = () => {                            
        const data = {
        id: estado.name,
        titulo: estado.title,
        completado: estado.completed

        };
        const insertar = axios.post(`https://jsonplaceholder.typicode.com/posts`, { data })
        
        .then(res => {
            console.log(res);
            console.log(res.data);
        })

        console.log(insertar)
    }

        
    const usersUrl = 'https://jsonplaceholder.typicode.com/users'
    const [usuarios, setUsuarios] = useState()
    const [usuario, setUsuario] = useState({"id": '',"name": "","username": "","email": ""})
    const [showUser, setShowUser] = useState(false);

    const infoUser = (user) => {
        setUsuario(user)
        setShowUser(true)
    };

    const handleCloseUser = () => setShowUser(false);

    const fetchApi = async () => {
        const response = await fetch(usersUrl)
        const responseJSON = await response.json()
        setUsuarios(responseJSON)
    }
    
    const [showTodos, setShowTodos] = useState(false);
    const closeTodos = () => setShowTodos(false);

    const handleShowTodos = () => {
        setShowTodos(true)
    }

    const [todos, setTodos] = useState()
    const getTodos = async (userId) => {
        const responseTodos = await fetch(`https://jsonplaceholder.typicode.com/users/${userId}/todos`)
        const responseTodosJSON = await responseTodos.json()
        setTodos(responseTodosJSON)
    }
        
    const [showPosts, setShowPosts] = useState(false);
    const handleClosePosts = () => setShowPosts(false);

    const handleShowPosts = () => {
        setShowPosts(true)
    };

    const [posts, setPosts] = useState()
    const fetchPosts = async (userId) => {
        const responsePost = await fetch(`https://jsonplaceholder.typicode.com/users/${userId}/posts`)
        const responsePostJSON = await responsePost.json()
        setPosts(responsePostJSON)
    }

    const [showComments, setShowComments] = useState(false);
    const handleCloseComments = () => setShowComments(false);

    const handleShowComments = () => {
        setShowComments(true)
    };

    const [comments, setComments] = useState()
    const fetchComments = async (postId) =>{
        const responseComment = await fetch(`https://jsonplaceholder.typicode.com/post/${postId}/comments`)
        const responseCommentJSON = await responseComment.json()
        setComments(responseCommentJSON)
    }
    
    const [showForm, setShowForm] = useState(false);
    const handleCloseForm = ( function () {
        alert("Enviando datos a: https://jsonplaceholder.typicode.com/posts")
        const data = {
            id: estado.name,
            titulo: estado.title,
            completado: estado.completed
    
            };
            const insertar = axios.post(`https://jsonplaceholder.typicode.com/posts`, { data })
        setShowForm(false) 
        setShowTodos(false)
        setShowUser(false)
    }) ;

    const handleShowForm = () =>{
        setShowForm(true)
    }       


    useEffect(() => {
        fetchApi()
    }, []);

    return (
        <div className="App">
        

        <Modal show={showUser} onHide={handleCloseUser}>
            <Modal.Header closeButton>
            <Modal.Title className='titulo'>Usuario: {usuario.name}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <Table borderless striped>
            <thead>
                <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Nombre de Usuario</th>
                <th>Correo</th>
                </tr>
            </thead>
            <tbody className='table-group-divider'>
                <tr>
                <td>{usuario.id}</td>
                <td>{usuario.name}</td>
                <td>{usuario.username}</td>
                <td>{usuario.email}</td>
                </tr>
            </tbody>
            </Table>
            </Modal.Body>
            <Modal.Footer>
            <Button variant="primary" onClick={()=> {fetchPosts(usuario.id); handleShowPosts(usuario);}}>
                Post's
            </Button>
            <Button variant="secondary" onClick={()=> {getTodos(usuario.id); handleShowTodos()}}>
                Todos
            </Button>
            </Modal.Footer>
        </Modal>
     {/* Post's */}
        <Modal show={showPosts} onHide={handleClosePosts}>
            <Modal.Header closeButton>
                <Modal.Title>Post's</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            { !posts ? 'Cargando Información......' :
                posts.map( (ItemPost,index) =>{
                    return(
                    <div>
                        <ul>
                        <li className='post'>Titulo: {ItemPost.title}</li>
                        </ul>
                    <div className='text-center'>
                    <Button variant="info" onClick={()=> {fetchComments(ItemPost.id); handleShowComments();}}>
                        Ver Comentarios
                        </Button>
                    </div>                    
                        <hr></hr>
                    </div>)
                })
            }
            </Modal.Body>
            <Modal.Footer>            
                <Button variant="danger" onClick={handleClosePosts}>
                    Cerrar
                </Button>
            </Modal.Footer>
        </Modal>
        {/* Post's */}        

        <Modal show={showTodos} onHide={closeTodos}>
        <Modal.Header closeButton>
        <Modal.Title className='titulo'>Todos</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        { !todos ? 'Cargando Información...' :
            todos.slice(0).reverse().map((ItemTodos,index) =>{
                return(
                <ul>
                    <li className='todos'>ID: {ItemTodos.id} - {ItemTodos.title}</li>
                    <hr></hr>
                </ul>)
            })
        }
        </Modal.Body>
            <Modal.Footer>
            <Button variant="primary" onClick={handleShowForm}>
                Formulario
            </Button>
            <Button variant="danger" onClick={closeTodos}>
                Cerrar
            </Button>            
            </Modal.Footer>
        </Modal>
    {/* Formulario */}            
        <Modal show={showForm} onHide={handleCloseForm}>
            <Modal.Header closeButton>
            <Modal.Title className='titulo'>Envio de datos</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <Form>
                <form className="mb-3" onSubmit={sendForm}>
                <label>
                    Id:
                    <input class="form-control" type="number" name="id" onChange={handleChange} required/>
                    Titulo:
                    <input class="form-control" type="text" name="title" onChange={handleChange} required/>
                    completed:
                    <input class="form-check-input" type="checkbox" name="completed" onChange={handleChange} />
                </label>
                <Button variant="success" type="button" onClick={handleCloseForm}>Enviar
                </Button>
                </form>
            </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="danger" onClick={handleCloseForm}>
                    Cerrar
                </Button>
            </Modal.Footer>
        </Modal>
    {/* Formulario */}



        <Modal show={showComments} onHide={handleCloseComments}>
            <Modal.Header closeButton>
            <Modal.Title>Comentarios</Modal.Title>
            </Modal.Header>
            <Modal.Body>{ !comments ? 'Cargando Información...' :
                    comments.map( (comment,index) =>{
                    return <tr style={{ color: 'black' }}>{comment.id}.- {comment.name}</tr>
                    })
                }</Modal.Body>
            <Modal.Footer>
            <Button variant="danger" onClick={handleCloseComments}>
                Cerrar
            </Button>
            </Modal.Footer>
        </Modal>

        <div className='container' style={{ marginTop: '2%' }}>           
            <Table >
            <tbody>
                { !usuarios ? 'Cargando Información' :
                    usuarios.map( (item,index) =>{
                    return <tr onClick={()=> infoUser(item)}><td className='item'>{item.id} | {item.name}</td></tr>
                    })
                }
                </tbody>
            </Table> 
        </div>

        </div>
    );
        
    } 

    export default Main