import React from 'react';
import * as ReactDOM from 'react-dom';
import Main from './App/App'
import Ventana from './Modal'


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<React.StrictMode>
                <Main />
        </React.StrictMode>);
/* const root1 = ReactDOM.createRoot(document.getElementById('modal'));
root.render(<React.StrictMode>
                <Ventana />
        </React.StrictMode>);   */                      